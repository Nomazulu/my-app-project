import 'package:flutter/material.dart';


class Dashboard extends StatefulWidget{
  const Dashboard({Key? key}) : super(key: key);


  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("INDHOLD"),
          elevation: .1,
          backgroundColor: const Color.fromRGBO(49, 87, 110, 1.0),
        ),
      );
       // ignore: unused_label, dead_code
       Body: Container(padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 2.0),
       child: GridView.count(
         crossAxisCount: 2,
        padding: const EdgeInsets.all(3.0),
        children:<Widget> [
          makeDashboardItem("Clothes", Icons.book),
          makeDashboardItem("Shoes", Icons.book),
          makeDashboardItem("Handbags", Icons.book),
          makeDashboardItem("Toys", Icons.book),
          makeDashboardItem("Food", Icons.book),
          makeDashboardItem("Art", Icons.book),
        ],
         
         ),
       );
 
 }
}

Card makeDashboardItem(String tittle,IconData icon){
  
  return Card(
    elevation: 1.0,
    margin: const EdgeInsets.all(8.0),
    
    child:  InkWell(
      onTap: (){},
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        verticalDirection: VerticalDirection.down,
        children:<Widget> [
          const SizedBox(height: 50.1,),
          Center(
            child:Icon(
              icon,
              size: 40.0,
              color: Colors.black,  
            )
                )
        ],
      ),
),
  );
}